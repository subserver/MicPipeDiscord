var opus = require('opusscript');
const Discord = require('discord.js');
const client = new Discord.Client();
const mic = require('mic');
const settings = require("./settings");

var micInstance = mic({
    rate: '16000',
    channels: '1',
    exitOnSilence: 0
});

var micInputStream = micInstance.getAudioStream();

client.login(settings.bot_key);
console.log("Logged In!");
client.on('message', async message => {
    if(!message.guild) return;
    if(message.content === '/join'){
        if(message.member.voiceChannel){
            message.reply("Attempting to join");
            const connection = await message.member.voiceChannel.join();
            connection.playStream(micInputStream);
        
        } else {
            message.reply("You need to join a channel first.");
        }
    }
});
 
micInputStream.on('error', function(err) {
    cosole.log("Error in Input Stream: " + err);
});

micInputStream.on('stopComplete', function() {
    console.log("Got SIGNAL stopComplete");
});

client.on("error", console.error);
micInstance.start();